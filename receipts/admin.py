from django.contrib import admin
from receipts.models import ExpenseCategory, Receipt, Account


@admin.register(ExpenseCategory)
class ExpenseCategory(admin.ModelAdmin):
    list_display = (
        "id",
        "name",
        "owner",
    )


@admin.register(Receipt)
class Recipt(admin.ModelAdmin):
    list_display = (
        "id",
        "vendor",
        "total",
        "tax",
        "date",
        "purchaser",
        "category",
        "account",
    )


@admin.register(Account)
class Account(admin.ModelAdmin):
    list_display = (
        "id",
        "name",
        "number",
        "owner",
    )
