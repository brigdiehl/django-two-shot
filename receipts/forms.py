from django.forms import ModelForm
from django.contrib.auth.decorators import login_required
from receipts.models import Receipt, ExpenseCategory



class ReceiptForm(ModelForm):
    class Meta:
        model = Receipt
        fields = [
            "vendor",
            "total",
            "tax",
            "date",
            "category",
            "account",
        ]


class CategoryForm(ModelForm):
    class Meta:
        model = ExpenseCategory
        fields = [
            "name",
        ]
